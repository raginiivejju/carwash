package com.carwash.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by i5 on 06-10-2018.
 */

public class CommonListModel {
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("vendorname")
    @Expose
    private String vendorname;
    @SerializedName("vendoremail")
    @Expose
    private String vendoremail;
    @SerializedName("vendormobile")
    @Expose
    private String vendormobile;
    @SerializedName("shop_name")
    @Expose
    private String shopName;
    @SerializedName("shop_address")
    @Expose
    private String shopAddress;
    @SerializedName("shop_image")
    @Expose
    private String shopImage;
    @SerializedName("shop_description")
    @Expose
    private String shopDescription;
    @SerializedName("shoplatitude")
    @Expose
    private String shoplatitude;
    @SerializedName("shoplongitude")
    @Expose
    private String shoplongitude;
    private String vendorid;
    private String favourite_id;

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getVendorname() {
        return vendorname;
    }

    public void setVendorname(String vendorname) {
        this.vendorname = vendorname;
    }

    public String getVendoremail() {
        return vendoremail;
    }

    public void setVendoremail(String vendoremail) {
        this.vendoremail = vendoremail;
    }

    public String getVendormobile() {
        return vendormobile;
    }

    public void setVendormobile(String vendormobile) {
        this.vendormobile = vendormobile;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getShopImage() {
        return shopImage;
    }

    public void setShopImage(String shopImage) {
        this.shopImage = shopImage;
    }

    public String getShopDescription() {
        return shopDescription;
    }

    public void setShopDescription(String shopDescription) {
        this.shopDescription = shopDescription;
    }

    public String getShoplatitude() {
        return shoplatitude;
    }

    public void setShoplatitude(String shoplatitude) {
        this.shoplatitude = shoplatitude;
    }

    public String getShoplongitude() {
        return shoplongitude;
    }

    public void setShoplongitude(String shoplongitude) {
        this.shoplongitude = shoplongitude;
    }

    public String getVendorid() {
        return vendorid;
    }

    public void setVendorid(String vendorid) {
        this.vendorid = vendorid;
    }

    public String getFavourite_id() {
        return favourite_id;
    }

    public void setFavourite_id(String favourite_id) {
        this.favourite_id = favourite_id;
    }
}
