package com.carwash.validateui;

 
public interface Validation {

    String getErrorMessage();

    boolean isValid(String text);

}