package com.carwash.constants;

public interface Event {
    int customerlogin = 1;
    int customerregistration = 2;
    int nearshops = 3;
    int forgotpassword = 4;
    int resetpassword = 5;
    int getfavourites = 6;
    int addfavourite = 7;
    int removefavourites = 8;
    int getprofile = 9;
    int customerprofileupdate = 10;
    int searchservices = 11;
    int changepassword = 12;
}
