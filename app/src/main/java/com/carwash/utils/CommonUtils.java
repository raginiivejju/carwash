package com.carwash.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

/**
 * Created by CIS16 on 28-Dec-17.
 */

public class CommonUtils {
    /**
     * @param tag
     * @param message
     */
    public static void logPrint(String tag, String message) {
        Log.e(tag, "" + message);
    }

    public static void showSnackBar(View mView, String message){
         Snackbar.make(mView,message, Snackbar.LENGTH_SHORT).show();
    }

    public static void showSnackBarLong(View mView, String message){
         Snackbar.make(mView,message, Snackbar.LENGTH_LONG).show();
    }

    /**
     * @param context
     * @param message
     */
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void makeCall(Context mContext, String Number){
        /*Checking is device have SimSlot or Not*/
        if (isPackageManagerIntentNull(mContext, new Intent(Intent.ACTION_DIAL)))
            mContext.startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + Number)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public static boolean isPackageManagerIntentNull(Context mContext, Intent mIntent) {
        PackageManager packageManager = mContext.getPackageManager();
        if (mIntent.resolveActivity(packageManager) != null) {
            return true;
        }

        return false;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html,String normal) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml("<b>"+html+"</b>"+normal, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml("<b>"+html+"</b>"+normal);
        }
        return result;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html){
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    public static boolean isGPSEnabled(Context mContext) {
        LocationManager lm = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            return false;
        } else {
            return true;
        }
    }

}
