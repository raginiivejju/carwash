package com.carwash.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.carwash.R;
import com.carwash.constants.AppConstants;
import com.carwash.constants.Event;
import com.carwash.ui.adapter.ServicePointsAdapter;
import com.carwash.ui.fragment.CarWashBaseFragment;
import com.carwash.ui.fragment.ProfileFragment;
import com.carwash.utils.AlertDialogHelper;
import com.carwash.utils.PrefUtil;

import carwash.utils.ToastUtils;

/**
 * Created by i5 on 01-10-2018.
 */

public class AccountFragment extends CarWashBaseFragment implements View.OnClickListener, AlertDialogHelper.AlertDialogListener {
    private View rootView;
    private Context mContext;
    private TextView textView_myProfile,textView_changePassword,textView_signOut;
    private AlertDialogHelper alertDialog;
    private FragmentTransaction ft;

    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.account, container, false);
        mContext = getActivity();
        initViews();
        setViews();
        return rootView;
    }

    private void initViews() {
        setToolBarHeading(getString(R.string.profile));
        alertDialog = new AlertDialogHelper(mContext);
        alertDialog.setOnAlertListener(this);
        textView_myProfile = (TextView) rootView.findViewById(R.id.textView_myProfile);
        textView_changePassword = (TextView) rootView.findViewById(R.id.textView_changePassword);
        textView_signOut = (TextView) rootView.findViewById(R.id.textView_signOut);
    }

    private void setViews() {
        textView_myProfile.setOnClickListener(this);
        textView_changePassword.setOnClickListener(this);
        textView_signOut.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.textView_myProfile:
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.container, new ProfileFragment());
                ft.addToBackStack(null);
                ft.commit();
                break;

            case R.id.textView_changePassword:
                startActivity(new Intent(mContext,ChangePasswordActivity.class).putExtra(AppConstants.intent_ComingFrom,AppConstants.intent_changepassword));
                break;

            case R.id.textView_signOut:
                alertDialog.showAlertDialog("", "Are you sure you want to exit?", "Ok", "Cancel", 0, true);
                break;
        }
    }

    @Override
    public void onPositiveClick(int from) {
        PrefUtil.putBool(mContext, AppConstants.PREF_firstTimeLogin, false, AppConstants.PREF_NAME);
        startActivity(new Intent(mContext,LoginActivity.class));
    }

    @Override
    public void onNegativeClick(int from) {

    }

    @Override
    public void onNeutralClick(int from) {

    }
}

