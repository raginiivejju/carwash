package com.carwash.ui.activiy;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.StrictMode;

import com.carwash.R;

import carwash.application.BaseApplication;
import carwash.ext.RequestManager;
import com.crashlytics.android.answers.Answers;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class CarWashBaseApplication extends BaseApplication {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.


    public static CarWashBaseApplication instance = null;

    private static Context context;

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @SuppressWarnings("unused")
    @Override
    public void onCreate() {

        if (false && Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().
                    detectAll().penaltyDialog().build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().
                    detectAll().penaltyDeath().build());
        }
        super.onCreate();
        Fabric.with(this, new Answers());
        instance = this;

        CarWashBaseApplication.context = getApplicationContext();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Font-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

    }

//    static {
//        HttpsURLConnection.setDefaultSSLSocketFactory(new SslFactory());
//    }

    @Override
    protected void initialize() {
        /***
         * Request manager for volley
         */

        RequestManager.initializeWith(this, new RequestManager.Config("data/data/predento/pics",
                5242880, 4));

    }


    public static Context getAppContext() {
        return CarWashBaseApplication.context;
    }

    public void onTerminate() {
        super.onTerminate();
    }

}
