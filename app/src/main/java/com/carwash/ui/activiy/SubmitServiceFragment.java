package com.carwash.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.carwash.R;
import com.carwash.constants.AppConstants;
import com.carwash.ui.fragment.CarWashBaseFragment;
import com.carwash.ui.fragment.ProfileFragment;
import com.carwash.utils.AlertDialogHelper;
import com.carwash.utils.PrefUtil;

public class SubmitServiceFragment extends CarWashBaseFragment implements View.OnClickListener, AlertDialogHelper.AlertDialogListener {
    private View rootView;
    private Context mContext;
    private AlertDialogHelper alertDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_submit_service, container, false);
        mContext = getActivity();
        initViews();
        setViews();
        return rootView;
    }

    private void initViews() {
        setToolBarHeading(getString(R.string.profile));
        alertDialog = new AlertDialogHelper(mContext);
        alertDialog.setOnAlertListener(this);
    }

    private void setViews() {
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }

    @Override
    public void onPositiveClick(int from) {

    }

    @Override
    public void onNegativeClick(int from) {

    }

    @Override
    public void onNeutralClick(int from) {

    }
}
