package com.carwash.ui.activiy;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.carwash.R;
import com.carwash.model.CommonListModel;
import com.carwash.utils.CommonUtils;

import java.util.ArrayList;

/**
 * Created by i5 on 06-10-2018.
 */

public class Example extends RecyclerView.Adapter<Example.MyHolder> {

    private Context mContext;
    private ArrayList<CommonListModel> mList;
    private Example.OnCartChangedListener onCartChangedListener;
    private View view;

    /**
     * Constructor provides information about, and access to, a single constructor for a class.
     *
     * @param mContext
     * @param mList
     */
    public Example(Context mContext, ArrayList<CommonListModel> mList) {
        this.mContext = mContext;
        this.mList = mList;

    }

    @Override
    public Example.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.activity_main, null);
        Example.MyHolder myHolder = new Example.MyHolder(view);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(final Example.MyHolder holder, final int position) {



    }


    @Override
    public int getItemCount() {
//        return (null != mSearchJobsModelList ? mSearchJobsModelList.size() : 0);
        return 5;
    }


    public class MyHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textName,textLocaton,rating;
        private View convertView;

        public MyHolder(View view) {
            super(view);
            convertView = view;
            imageView= (ImageView)itemView.findViewById(R.id.imagefav);
            textName=itemView.findViewById(R.id.textname);
            textLocaton=itemView.findViewById(R.id.textLocation);
            rating=itemView.findViewById(R.id.textrating);
        }
    }


    public void setOnCartChangedListener(Example.OnCartChangedListener onCartChangedListener) {
        this.onCartChangedListener = onCartChangedListener;
    }

    public interface OnCartChangedListener {
        void setCartClickListener(String status, int position);
    }


}
