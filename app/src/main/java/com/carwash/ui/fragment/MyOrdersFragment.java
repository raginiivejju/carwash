package com.carwash.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.carwash.R;
import com.carwash.ui.adapter.MyOrdersAdapter;

import carwash.utils.ToastUtils;

/**
 * Created by i5 on 01-10-2018.
 */

public class MyOrdersFragment extends CarWashBaseFragment implements MyOrdersAdapter.OnCartChangedListener {
    private View rootView;
    private Context mContext;
    private RecyclerView recyclerView;
    private MyOrdersAdapter adapter;


    public MyOrdersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.activity_car_service_point_fragment, container, false);
        mContext = getActivity();
        initViews();
        setViews();
        return rootView;
    }

    private void initViews() {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
    }

    private void setViews() {
        adapter = new MyOrdersAdapter(mContext);
        recyclerView.setAdapter(adapter);
        adapter.setOnCartChangedListener(this);
    }

    @Override
    public void setCartClickListener(String status, int position) {
        ToastUtils.showToast(mContext,""+position);
//        getSupportFragmentManager().beginTransaction().add(R.id.container, new JobsLIstFragment()).commit();
//        final FragmentTransaction ft = getFragmentManager().beginTransaction();
//        ft.replace(R.id.container, new JobDetailsFragment(), "NewFragmentTag");
//        ft.commit();
    }
}


