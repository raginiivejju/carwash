/*
 *
 *  Proprietary and confidential. Property of Calibrage info systems. Do not disclose or distribute.
 *  You must have written permission from Calibrage info systems. to use this code.
 *
 */

package carwash.ui.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import carwash.ui.IScreen;
import carwash.ui.activity.BaseActivity;


/**
 * @author sachn.guta
 */
public abstract class BaseFragment extends Fragment implements IScreen {

    /**
     * @return
     */
    protected BaseActivity getBaseActivity() {
        FragmentActivity activity = getActivity();
        if (!(activity instanceof BaseActivity) || activity.isFinishing()) {
            return null;
        }
        return (BaseActivity) activity;
    }

 }
